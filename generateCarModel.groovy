@Grab(group = 'net.sf.opencsv', module = 'opencsv', version = '2.3')
import au.com.bytecode.opencsv.*
@Grab(group = 'net.sf.opencsv', module = 'opencsv', version = '2.3')
import au.com.bytecode.opencsv.*

def carMap = ['Passat':'London', 'Golf' : 'London', 'Passat' : 'Bristol', 'Sharan' : 'Leeds', 'Golf': 'London']
def random = new Random();
def cars = carMap.keySet().toList()
int listSize =  cars.size()
Writer writer =new FileWriter("src/test/testFiles/carModelLarge.csv")
def w = new CSVWriter(writer,CSVWriter.DEFAULT_SEPARATOR, CSVWriter.NO_QUOTE_CHARACTER)
w.writeNext((String[]) ['User ID','Password'])
(1..1000).each{
 String[] line = (String[]) [ cars.get(random.nextInt(listSize)) , carMap.get(cars.get(random.nextInt(listSize)))]
   w.writeNext(line)
    println it
} 
writer.close()
println 'Done'