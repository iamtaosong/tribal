package uk.co.songt;


import com.google.inject.Inject;
import org.jukito.JukitoModule;
import org.jukito.JukitoRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import uk.co.songt.exception.FileContentExceptoion;
import uk.co.songt.imp.FileReaderImp;
import uk.co.songt.juice.module.ApplicationModule;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;

@RunWith(JukitoRunner.class)
public class SalesImpApplicationModuleTest {

    public static class TestModule extends JukitoModule {
        @Override
        protected void configureTest() {
            install(new ApplicationModule());
        }
    }

    @Inject
    private Sales sales;
    @Inject
    Properties properties;
    @Test(expected=FileContentExceptoion.class)
    public void testGetCarSales() {
          sales.getCarSales();
    }

    @Test(expected=FileContentExceptoion.class)
    public void testTotalSales() {
          sales.sales("golf");
    }

    @Test(expected=FileContentExceptoion.class)
    public void testMostPopularModelOf() {
          sales.mostPopularModelOf("london");
    }

    @Test(expected=FileContentExceptoion.class)
    public void testMostPopularModel() {
         sales.mostPopularModel();
    }

    @Test
    public void testAddSale()  throws IOException {
        final String fileName = properties.getProperty(PropertiesKeys.DATA_SOURCE);
        FileReaderImp fileReaderImp = new FileReaderImp();
        fileReaderImp.addToFile(fileName,"Model,City", true);
        CarSale[] allSales = sales.getCarSales();
        sales.addSale("Polo", "london");
        allSales = sales.getCarSales();
        int poloCount =  (int)Arrays.stream(allSales).filter(carSale -> carSale.getModel().equalsIgnoreCase("polo")).count();
        assert 1 ==poloCount;
        File file = new File(fileName);
        file.delete();
    }

    @Test(expected=FileContentExceptoion.class)
    public void testAddSaleException() {
        final String fileName = properties.getProperty(PropertiesKeys.DATA_SOURCE);
        sales.getCarSales();
    }
}
