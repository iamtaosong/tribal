package uk.co.songt;


import com.google.inject.Inject;
import org.jukito.JukitoModule;
import org.jukito.JukitoRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import uk.co.songt.module.ApplicationModuleTextContext;

import java.util.Arrays;
import java.util.Properties;
import java.util.stream.Stream;

@RunWith(JukitoRunner.class)
public class SalesImpTest {

    public static class TestModule extends JukitoModule {
        @Override
        protected void configureTest() {
            install(new ApplicationModuleTextContext());
        }
    }
    @Inject
    private Sales sales;
    @Inject
    Properties properties;
    final String LONDON="london";

    @Test
    public void testGetCarSales() {
        assert sales.getCarSales().length==5;
    }

    @Test
    public void testTotalSales() {
        assert sales.sales("golf")==2;
    }

    @Test
    public void testMostPopularModelOf() {
        String[] popularModels  = sales.mostPopularModelOf(LONDON);
        assert Arrays.stream(popularModels).anyMatch("Golf"::equals);
    }

    @Test
    public void testMostPopularModel() {
        String[] popularModels  = sales.mostPopularModel();
        assert Arrays.stream(popularModels).anyMatch("Passat"::equals);
        assert Arrays.stream(popularModels).anyMatch("Golf"::equals);
    }


}
