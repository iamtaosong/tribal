package uk.co.songt;

import com.google.inject.Guice;
import com.google.inject.Injector;
import uk.co.songt.juice.module.ApplicationModule;

public class Application {

    public static void main(String[] args){
        Injector injector = Guice.createInjector(new ApplicationModule());
    }
}
