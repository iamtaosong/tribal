package uk.co.songt;

/**
 * Created by tsong on 28/06/2016.
 */
public interface Sales {

    public CarSale[] getCarSales();

    public String[] getUnqueItems( Object[] modles );

    public String[] mostPopularModel();

    public String[] mostPopularModelOf(String city);

    public int sales(String model);

    public void addSale(String model,String city);
}
