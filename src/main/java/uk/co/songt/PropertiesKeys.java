package uk.co.songt;

public interface PropertiesKeys {

    public final String DATA_SOURCE = "data.source";
    public final String MODEL="Model";
    public final String CITY="City";
    public final String CSV_SPLITOR = ",";
}
