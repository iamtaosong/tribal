package uk.co.songt.imp;

import com.google.inject.Inject;
import uk.co.songt.CarSale;
import uk.co.songt.FileReader;
import uk.co.songt.PropertiesKeys;
import uk.co.songt.Sales;

import java.io.File;
import java.util.Arrays;
import java.util.Properties;
import java.util.stream.Stream;

public class SalesImp implements Sales {

    final String SPLITOR = "-";
    @Inject
    FileReader fileReader;
    @Inject
    Properties properties;

    public CarSale[] getCarSales() {
        return fileReader.parseFile(properties.getProperty(PropertiesKeys.DATA_SOURCE));
    }

    public String[] getUnqueItems(Object[] modles) {
        return Stream.of(modles).distinct().toArray(size -> new String[size]);
    }

    public  int getModelTotal(Object[] modles,String uniqueModel){
        return (int) Stream.of(modles).filter(modle -> modle.toString().equalsIgnoreCase(uniqueModel)).count();
    }

    public Object[] getModelWithCount( String[] uniqueModelCount ,int max){
        return Stream.of(uniqueModelCount).filter(model -> model.contains(String.valueOf(max))).map(m -> m.split(SPLITOR)[0]).toArray();
    }

    public String[]  getStringArrayFromObjectArray(Object[] popularModelsObject){
        String[] popularModels = new String[popularModelsObject.length];
        int index = 0;
        for (Object object : popularModelsObject) {
            popularModels[index] = (String) object;
            index++;
        }
        return popularModels;
    }
    public String[] getTopItems(Object[] modles) {
        String[] uniqueModels = getUnqueItems(modles);
        int[] modelCount = new int[uniqueModels.length];
        String[] uniqueModelCount = new String[uniqueModels.length];
        int index = 0;
        for (String uniqueModel : uniqueModels) {
            int modelTotal =getModelTotal(modles, uniqueModel);
            modelCount[index] = modelTotal;
            uniqueModelCount[index] = uniqueModel + SPLITOR + modelTotal;
            index++;
        }
        final int max = Arrays.stream(modelCount).max().getAsInt();
        Object[] popularModelsObject = getModelWithCount(uniqueModelCount,max);
        return getStringArrayFromObjectArray(popularModelsObject);
    }

    public String[] mostPopularModel() {
        CarSale[] carSales = getCarSales();
        Object[] modles = Stream.of(carSales).map(i -> i.getModel()).toArray();
        return getTopItems(modles);
    }

    public String[] mostPopularModelOf(String city) {
        CarSale[] carSales = getCarSales();
        Object[] modles = Stream.of(carSales).filter(carSale -> carSale.getCity().equalsIgnoreCase(city))
                .map(i -> i.getModel()).toArray();
        return getTopItems(modles);
    }

    public int sales(String model) {
        CarSale[] carSales = getCarSales();
        int modelSale = (int) Stream.of(carSales).filter(carSale -> carSale.getModel().equalsIgnoreCase(model)).count();
        return modelSale;
    }

    public void addSale(String model, String city) {
        File file = new File(properties.getProperty(PropertiesKeys.DATA_SOURCE));
        String entry = model+ PropertiesKeys.CSV_SPLITOR+city;
        if (!file.exists()) {
            String titleEntry = PropertiesKeys.MODEL + PropertiesKeys.CSV_SPLITOR + PropertiesKeys.CITY;
            fileReader.addToFile(properties.getProperty(PropertiesKeys.DATA_SOURCE), titleEntry, true);
        }
        fileReader.addToFile(properties.getProperty(PropertiesKeys.DATA_SOURCE), entry,false);
    }
}
