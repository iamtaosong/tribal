package uk.co.songt.juice.module;


import com.google.inject.Binder;
import com.google.inject.Module;
import uk.co.songt.FileReader;
import uk.co.songt.Sales;
import uk.co.songt.imp.FileReaderImp;
import uk.co.songt.imp.SalesImp;

public class ServiceModule implements Module {

    @Override
    public void configure(Binder binder) {
        binder.bind(FileReader.class).to(FileReaderImp.class).asEagerSingleton();
        binder.bind(Sales.class).to(SalesImp.class).asEagerSingleton();

    }


}
